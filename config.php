root@3ff7809d5c12:/var/www/html# cat config/config.php 
<?php
$CONFIG = array(
  'htaccess.RewriteBase' => '/',
  'memcache.local' => '\\OC\\Memcache\\APCu',
  'apps_paths' => 
  array (
    0 => 
    array (
      'path' => '/var/www/html/apps',
      'url' => '/apps',
      'writable' => false,
    ),
    1 => 
    array (
      'path' => '/var/www/html/custom_apps',
      'url' => '/custom_apps',
      'writable' => true,
    ),
  ),
  'instanceid' => 'ocdcajynutx7',
  'passwordsalt' => 'zE9d4xDQ5MKlHs6XJe9U9mO0CFb3yF',
  'secret' => 'OkL6h5d2EJEUKIAfdsSaKEzNte1WW6Ve3REX2zZXmCp7eff5',
  'trusted_domains' => 
  array (
    0 => '0.0.0.0:8080',
  ),
  'datadirectory' => '/var/www/html/data',
  'dbtype' => 'pgsql',
  'version' => '27.0.1.2',
  'overwrite.cli.url' => 'http://0.0.0.0:8080',
  'dbname' => 'nextcloud',
  'dbhost' => 'postgres', // Utilisez le nom du service Docker pour PostgreSQL
  'dbport' => '',
  'dbtableprefix' => 'oc_',
  'dbuser' => 'nextcloud',
  'dbpassword' => 'your_postgres_password', // Utilisez le mot de passe PostgreSQL que vous avez défini dans le fichier envFile
  'installed' => true,

  // Ajouter les configurations spécifiques à Collabora Online ici
  'officeonline' => array(
    'allow_local_remote_servers' => true,
    'wopi_url' => 'http://172.20.0.14:9980', // Adresse du serveur Collabora Online
    'disable_certificate_verification' => true, // Utilisez cette option si vous avez des problèmes de certificat HTTPS avec Collabora
  ),
);

